import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.function.BiConsumer;

public class HashCode2021Main {
    HashMap<String,Street> getStreetsByName = new HashMap<>();
    HashMap<Integer,Intersection> getIntersectionsByNumber = new HashMap<>();
    PriorityQueue<Street> streetsPQ = new PriorityQueue<>();
    ArrayDeque<Intersection> intersectionsWithSchedules = new ArrayDeque<>();
    int intesectionWithSchedulesCount = 0;
//    PriorityQueue<C>
    public class Car{
        int arrivalTime, timeSpentCrossing;
        //How long has the car beed at the edgepark
        boolean freshlyAdded;
        ArrayDeque<String> path = new ArrayDeque<>();
        public Car(){
            timeSpentCrossing = 0;
        }
    }
    public class Light{
        public int seconds;
        public String color;
        public String street;
        public Light(String color,String street, int seconds){
            this.color = color;
            this.seconds = seconds;
            this.street = street;
        }
        public String fromStreet;
    }
    public class Street implements Comparable<Street>{
        String name;
        int start, end, streetCrossingTime;
        ArrayDeque<Car> carLineUp = new ArrayDeque<>();
        ArrayDeque<Car> endQueue = new ArrayDeque<>();
        ArrayDeque<Car> startQueue = new ArrayDeque<>();
        public Street(){ }
        public int compareTo(Street s){
            if (this.streetCrossingTime < s.streetCrossingTime){
                return 1;
            } else if (this.streetCrossingTime > s.streetCrossingTime){
                return -1;
            }else return 0;
        }
    }
    public class ByCrossingTime implements Comparator<Street>{
        @Override
        public int compare(Street street1, Street street2) {
            if (street1.streetCrossingTime < street2.streetCrossingTime){
                return 1;
            } else if (street1.streetCrossingTime > street2.streetCrossingTime){
                return -1;
            }else return 0;
        }
    }
    public class ByEdgeQueue implements Comparator<Street>{
        @Override
        public int compare(Street street1, Street street2) {
            if (street1.endQueue.size() + street1.startQueue.size() < street2.endQueue.size() + street2.startQueue.size()){
                return 1;
            } else if (street1.endQueue.size() + street1.startQueue.size() > street2.endQueue.size() + street2.startQueue.size()){
                return -1;
            }else return 0;
        }
    }

    public class Intersection{
        public class Schedule{
            PriorityQueue<Light> streets = new PriorityQueue<>();
            ArrayDeque<Light> lightOrder;
            HashMap<String, Integer> lightOrderTime = new HashMap<>();
            public Schedule(){
                this.lightOrder = new ArrayDeque<>();
            }
            public PriorityQueue<Schedule> neighbours(){
                return new PriorityQueue<>();
            }
        }
//        PriorityQueue<>
        Schedule schedule;
        public Intersection(){
            this.schedule = new Schedule();
        }
        Comparator byCrossingTime = new ByCrossingTime();
        Comparator byEdgeQueue = new ByEdgeQueue();
        PriorityQueue<Street> outflowStreets = new PriorityQueue<>(1, byCrossingTime);
        PriorityQueue<Street> inflowStreets = new PriorityQueue<>(1, byEdgeQueue);
//        HashMap<String, Schedule> streetSchedules = new HashMap();
    }

    public static void main(String [] args) throws FileNotFoundException {
        HashCode2021Main hashCode2021Main = new HashCode2021Main();
        hashCode2021Main.run();
    }

    public void run() throws FileNotFoundException {
        File file = new File("/home/lobi/IdeaProjects/I/src/d.txt");
        Scanner input = new Scanner(file);
        int simulationDuration = input.nextInt();
        int intersectionCount = input.nextInt();
        int streetCount = input.nextInt();
        int carCount = input.nextInt();
        int bonusPoints = input.nextInt();
        for (int i = 0; i < intersectionCount; i++){
            Intersection intersection = new Intersection();
            System.out.println("Creating intersection "+i);
            getIntersectionsByNumber.put(i, intersection);
        }
        for (int i = 0; i < streetCount; i++){
            Street street = new Street();
            street.start = input.nextInt();
            street.end = input.nextInt();
            street.name = input.next();
            street.streetCrossingTime = input.nextInt();
            streetsPQ.add(street);
            getStreetsByName.put(street.name, street);
            getIntersectionsByNumber.get(street.start).outflowStreets.add(street);
            System.out.println("Adding street "+street.name+" to inflows at intersection "+street.end);
            getIntersectionsByNumber.get(street.end).inflowStreets.add(street);
//            getIntersectionsByNumber.get(street.end).streetSchedules.put();
        }
        System.out.println("Number of streets added: "+streetsPQ.size());
        for (int i = 0; i < carCount; i++){
            Car car = new Car();
            int numberOfStreetsToBeTravelled = input.nextInt();
            int tCT = 0;
            String startingStreetName = "";
            for (int j = 0; j < numberOfStreetsToBeTravelled; j++){
                String streetName = input.next();
                Street street = getStreetsByName.get(streetName);
                car.path.add(streetName);
                tCT = tCT + street.streetCrossingTime;
                //Register starting point
                if (j == 0) {
                    startingStreetName = streetName;
                    car.freshlyAdded = false;
                    street.endQueue.add(car);
                }
            }
            System.out.println("Car "+i+" starting at street "+startingStreetName+" has a path of length "+car.path.size());
        }
        System.out.println("--------------END--------------");


//        getIntersectionsByNumber.forEach((id, intersection) -> {
//            if (intersection.timeToFlop.isEmpty()) return;
//            intersection.timeToFlop.forEach((time) -> {
//
//            });
//        });



        int time = 0;
        while (time < simulationDuration){
//            System.out.println("Time T = "+time);
            //For each intersection ...
            getIntersectionsByNumber.forEach((id, intersection) -> {
//                System.out.println("Intersection "+id+" which has "+intersection.inflowStreets.size()+" inflowStreets");
//                System.out.println("intersection.inflowStreets.isEmpty(): "+intersection.inflowStreets.isEmpty());
                if (!intersection.inflowStreets.isEmpty()){
//                    System.out.println(""+intersection.inflowStreets.size()+" inflowStreets");
                    intersection.inflowStreets.forEach((street -> {
//                        System.out.print("STREET: "+street.name+" STARTQUEUE CARS: "+street.startQueue.size()+" INFLOWING CARS: "+street.carLineUp.size()+" ENDQUEUE CARS: "+street.endQueue.size());
//                        System.out.println();
                        //Move all cars in street
                        ArrayDeque<Car> carsToRemove = new ArrayDeque<>();
                        street.carLineUp.forEach((car -> {
                            car.timeSpentCrossing += 1;
                            if (car.timeSpentCrossing ==street.streetCrossingTime){
//                                System.out.println("Adding car to edge queue at street "+street.name);
//                                street.endQueue.add(street.carLineUp.poll());
                                carsToRemove.add(car);
                            }
                        }));
                        carsToRemove.forEach(car -> {
//                            System.out.println("Adding car to edge queue at street "+street.name+" after removing it from carLineUp of size "+street.carLineUp.size());
                            street.carLineUp.remove(car);
                            car.freshlyAdded = true;
                            street.endQueue.add(car);
                        });
                        street.startQueue.forEach(car -> {
                            if (id == 2)
//                                System.out.println("Adding car to lineup in street "+street.name);
                            // Necessary ??
                            //car.timeSpentCrossing += 1;
                            street.carLineUp.add(car);
                        });
                        street.startQueue = new ArrayDeque<>();
                    }));
//                    System.out.println();
                    // ... get the best street to open.
                    Street candidateStreet = intersection.inflowStreets.peek();
                    //For cars that are the end of their journey, remove them from the endQueue
//                    ArrayDeque<Car> carsToRemove = new ArrayDeque<>();
//                    Iterator<Car> itCar = candidateStreet.endQueue.iterator();
//                    while (itCar.hasNext()){
//                        Car c = itCar.next();
//                        if (c.path.isEmpty())
//                            carsToRemove.add(c);
//                    }
//                    carsToRemove.forEach(car -> {
//                        if (!car.freshlyAdded)
//                            candidateStreet.endQueue.remove(car);
//                        else car.freshlyAdded = false;
//                    });
                    //Move car from candidate street edge queue to new street for the others
                    if (!candidateStreet.endQueue.isEmpty()) {
//                        System.out.println("candidateStreet " + candidateStreet.name+" endQueue.size() " + candidateStreet.endQueue.size());
                        Car car = candidateStreet.endQueue.pollFirst();
                        //Reorder the street's importance after removing a car
                        intersection.inflowStreets.remove(candidateStreet);
                        intersection.inflowStreets.add(candidateStreet);
//                        printCar(car);
//                        System.out.println("car.path.size() " + car.path.size());
//                        sida...
                        //Remove current street
                        if (!(car.path.isEmpty()))
                            car.path.pollFirst();
                        //Move car to next street
                        if (!(car.path.isEmpty())) {
                            Street st = getStreetsByName.get(car.path.pollFirst());
//                            System.out.print("We're removing from endqueue at "+candidateStreet.name+" and adding to startqueue of "+st.name);
                            car.timeSpentCrossing = 0;
                            st.startQueue.add(car);
                        }
//                        else ...
                        //Adjust the street's schedule accordingly
                        intersection.schedule.lightOrder.add(new Light("green", candidateStreet.name, 1));
                        Integer numOfSecondsOn =intersection.schedule.lightOrderTime.get(candidateStreet.name);
                        if (numOfSecondsOn == null)
                            intersection.schedule.lightOrderTime.put(candidateStreet.name, 1);
                        else intersection.schedule.lightOrderTime.put(candidateStreet.name, numOfSecondsOn+1);
                        //                    intersection.scheduleCandiates.neighbours().forEach((n) -> {
                        //                       n.
                        //                    });
//                        System.out.println();
                    }
                }
//                System.out.println();
//                System.out.println();
            });
//            System.out.println();
//            System.out.println();
//            System.out.println();
            time++;
        }
        System.out.println("!!!");
        getIntersectionsByNumber.forEach((id, intersection) -> {
                if (!intersection.schedule.lightOrder.isEmpty()) {
                    intesectionWithSchedulesCount++;
                }
            });
        System.out.println(intesectionWithSchedulesCount);
        getIntersectionsByNumber.forEach((id, intersection) -> {
//            System.out.println("Printing intersection "+id);

//                System.out.println("Printing schedule "+id);
                if (!intersection.schedule.lightOrder.isEmpty()){
                    System.out.println(id);
                    System.out.println(intersection.schedule.lightOrderTime.size());
                    TreeSet<String> printedStreets = new TreeSet<>();
                    intersection.schedule.lightOrder.forEach((greenLight -> {
                        if (printedStreets.contains(greenLight.street));
                        else System.out.println(greenLight.street +" "+ intersection.schedule.lightOrderTime.get(greenLight.street));
                        printedStreets.add(greenLight.street);
                    }));
                }
        });

    }

    public void printCar(Car c){
        System.out.println("|Car. Next Street :: "+c.path.peek());
    }
}
