import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.*;

class HashCode2021Test {
    public float ingredientAppearances = 0;
    //for use in rarity calculation
    Delivery seedDelivery = new Delivery();

    HashMap<String, Integer> ingredientTypeCount = new HashMap<>();

    PriorityQueue<Delivery> deliveriesPQ = new PriorityQueue();

    public class Team implements Comparable<Team>{
        int size;
        int diversityIndex;
        PriorityQueue<Pizza> allocatedPizzas;
        TreeSet<String> ingredientsAlreadyServed = new TreeSet<>();
        public Team(int size){
            this.size = size; allocatedPizzas = new PriorityQueue();
        }

        public void allocatePizza(Pizza p){
            allocatedPizzas.add(p);
        }

        public Pizza removePizza(){
            Pizza p = allocatedPizzas.poll();
            return p;
        }

        public void calculateDiversityIndex(){ }

        public int compareTo(Team t){
            double rand = Math.random();
            if (rand > 0.5){
                return 1;
            } else return -1;
        }
    }

    public class Pizza implements Comparable<Pizza> {
        TreeSet<String> ingredients = new TreeSet<>();
        int index;
        public Pizza(int index){
            this.index = index;
        }
        double rarity;

        public void addIngredient(String ingredient){
            ingredients.add(ingredient);
        }

        public int compareTo(Pizza p){
            if(this.ingredients.size() == p.ingredients.size())
            {
                if (this.rarity > p.rarity){
                    return  -1;
                } else if (this.rarity < p.rarity){
                    return 1;
                } else return 0;
            }
            else if(this.ingredients.size() > p.ingredients.size())
                return -1;
            else return 1;
        }

        public void calculateRarity(){
            Iterator<String> itI = ingredients.iterator();
            while (itI.hasNext()){
                String ingredient = itI.next();
                int count = ingredientTypeCount.get(ingredient);
                rarity = rarity + 1 / (count / ingredientAppearances);
            }
        }
    }

    public class Delivery implements Comparable<Delivery>{
        public PriorityQueue<Team> unallocatedTeamsOfTwo, unallocatedTeamsOfThree,unallocatedTeamsOfFour;
        public PriorityQueue<Team> allocatedTeamsOfTwo, allocatedTeamsOfThree, allocatedTeamsOfFour;
        public PriorityQueue<Pizza> unallocatedPizzasPQ;
        public int numberOfPizzasRemaining;
        public int score;
        int spaceRemaining;
        boolean capturesAllPizzas;
        public Delivery(){
            unallocatedTeamsOfTwo =  new PriorityQueue<Team>();
            unallocatedTeamsOfThree  =  new PriorityQueue<Team>();
            unallocatedTeamsOfFour =  new PriorityQueue<Team>();

            allocatedTeamsOfTwo  =  new PriorityQueue<Team>();
            allocatedTeamsOfThree  =  new PriorityQueue<>();
            allocatedTeamsOfFour  =  new PriorityQueue<>();
            unallocatedPizzasPQ =  new PriorityQueue<>();
        }

        public void calculateScore(){

        }

        public void calculateSpaceRemaining(){ }

        public int compareTo(Delivery d){
            return 1;
        }

        public void copyDeliveriesToChild(Delivery d){
            Iterator<Team> it;
            it = unallocatedTeamsOfTwo.iterator();
            while (it.hasNext()){
                d.unallocatedTeamsOfTwo.add(it.next());
            }
            it = unallocatedTeamsOfThree.iterator();
            while (it.hasNext()){
                d.unallocatedTeamsOfThree.add(it.next());
            }
            it = unallocatedTeamsOfFour.iterator();
            while (it.hasNext()){
                d.unallocatedTeamsOfFour.add(it.next());
            }

            it = allocatedTeamsOfTwo.iterator();
            while (it.hasNext()){
                d.allocatedTeamsOfTwo.add(it.next());
            }
            it = allocatedTeamsOfThree.iterator();
            while (it.hasNext()){
                d.allocatedTeamsOfThree.add(it.next());
            }
            it = allocatedTeamsOfFour.iterator();
            while (it.hasNext()){
                d.allocatedTeamsOfFour.add(it.next());
            }

            //Remove this ??
//            Iterator<Pizza> itP;
//            itP = unallocatedPizzasPQ.iterator();
//            while (itP.hasNext()){
//                d.unallocatedPizzasPQ.add(itP.next());
//            }
        }

        public PriorityQueue<Delivery> neighbors() {
            PriorityQueue<Delivery> neighbours = new PriorityQueue<>();
//            System.out.println("unallocatedPizzasPQ.size()"+unallocatedPizzasPQ.size());
            while(!unallocatedPizzasPQ.isEmpty()){
                if(unallocatedTeamsOfThree.isEmpty() && unallocatedTeamsOfTwo.isEmpty() && unallocatedTeamsOfFour.isEmpty())
                    return neighbours;
//                System.out.println("unallocatedPizzasPQ.size()"+unallocatedPizzasPQ.size());
                int remainingPizzasCount = unallocatedPizzasPQ.size();
                int maximumNumberOfFours = remainingPizzasCount/4;
                int numberOfThrees = (remainingPizzasCount%4)/3;
                int numberOfTwos = (remainingPizzasCount%4)/3;
                Delivery d = new Delivery();
                if (remainingPizzasCount >= 4){
//                    System.out.println("unallocatedTeamsOfFour.isEmpty()"+unallocatedTeamsOfFour.isEmpty());
                    if (!unallocatedTeamsOfFour.isEmpty()){
//                        System.out.println("inaingia hapa");
//                        System.out.println("reducing unallocatedPizzasPQ by one "+unallocatedPizzasPQ.size());
                        Pizza nextPizza1 = unallocatedPizzasPQ.poll();
//                        System.out.println("popped pizza"+nextPizza1.toString());
                        Pizza nextPizza2 = unallocatedPizzasPQ.poll();
//                        System.out.println("popped pizza"+nextPizza2.toString());
                        Pizza nextPizza3 = unallocatedPizzasPQ.poll();
//                        System.out.println("popped pizza"+nextPizza3.toString());
                        Pizza nextPizza4 = unallocatedPizzasPQ.poll();
//                        System.out.println("popped pizza"+nextPizza4.toString());
//                        System.out.println("unallocatedTeamsOfFour.size "+unallocatedTeamsOfFour.size());
//                        System.out.println("unallocatedTeamsOfFour inafaa kureduce after kupolliwa but haireduce");
                        Team nextTeamOfFour = unallocatedTeamsOfFour.poll();
//                        System.out.println("unallocatedTeamsOfFour.size "+unallocatedTeamsOfFour.size());

                        nextTeamOfFour.allocatePizza(nextPizza1);
                        nextTeamOfFour.allocatePizza(nextPizza2);
                        nextTeamOfFour.allocatePizza(nextPizza3);
                        nextTeamOfFour.allocatePizza(nextPizza4);
//                        System.out.println("nextTeamOfFour.ingredientsAlreadyServed.size()" + nextTeamOfFour.ingredientsAlreadyServed.size());
                        nextTeamOfFour.ingredientsAlreadyServed.addAll(nextPizza1.ingredients);
//                        System.out.println("nextTeamOfFour.ingredientsAlreadyServed.size()" + nextTeamOfFour.ingredientsAlreadyServed.size());
                        nextTeamOfFour.ingredientsAlreadyServed.addAll(nextPizza2.ingredients);
//                        System.out.println("nextTeamOfFour.ingredientsAlreadyServed.size()" + nextTeamOfFour.ingredientsAlreadyServed.size());
                        nextTeamOfFour.ingredientsAlreadyServed.addAll(nextPizza3.ingredients);
//                        System.out.println("nextTeamOfFour.ingredientsAlreadyServed.size()" + nextTeamOfFour.ingredientsAlreadyServed.size());
                        nextTeamOfFour.ingredientsAlreadyServed.addAll(nextPizza4.ingredients);
//                        System.out.println("nextTeamOfFour.ingredientsAlreadyServed.size()" + nextTeamOfFour.ingredientsAlreadyServed.size());
                        allocatedTeamsOfFour.add(nextTeamOfFour);
//                        System.out.println("nextTeamOfFour.ingredientsAlreadyServed.size()" + nextTeamOfFour.ingredientsAlreadyServed.size());
//                        System.out.println("d.score"+d.score);
                        d.allocatedTeamsOfFour.add(nextTeamOfFour);
                        copyDeliveriesToChild(d);
//                        System.out.println(nextTeamOfFour.ingredientsAlreadyServed.size() * nextTeamOfFour.ingredientsAlreadyServed.size());
//                        d.score = nextTeamOfFour.ingredientsAlreadyServed.size() * nextTeamOfFour.ingredientsAlreadyServed.size();
                        neighbours.add(d);
//                        System.out.println("unallocatedPizzasPQ.size()"+unallocatedPizzasPQ.size());
                    }
                    else if (!unallocatedTeamsOfThree.isEmpty()){
//                        System.out.println("reducing unallocatedPizzasPQ by one "+unallocatedPizzasPQ.size());
                        Pizza nextPizza1 = unallocatedPizzasPQ.poll();
                        Pizza nextPizza2 = unallocatedPizzasPQ.poll();
                        Pizza nextPizza3 = unallocatedPizzasPQ.poll();
//                            System.out.println("after reducing by one "+unallocatedPizzasPQ.size());
                        Team nextTeamOfThree = unallocatedTeamsOfThree.poll();
//                            System.out.println("Allocating pizza to a team of one");
                        nextTeamOfThree.allocatePizza(nextPizza1);
                        nextTeamOfThree.allocatePizza(nextPizza2);
                        nextTeamOfThree.allocatePizza(nextPizza3);
                        nextTeamOfThree.ingredientsAlreadyServed.addAll(nextPizza1.ingredients);
                        nextTeamOfThree.ingredientsAlreadyServed.addAll(nextPizza2.ingredients);
                        nextTeamOfThree.ingredientsAlreadyServed.addAll(nextPizza3.ingredients);

                        allocatedTeamsOfThree.add(nextTeamOfThree);
                        copyDeliveriesToChild(d);
                        d.score += nextTeamOfThree.ingredientsAlreadyServed.size();
                        neighbours.add(d);
                    } else if (!unallocatedTeamsOfTwo.isEmpty()){
//                        System.out.println("reducing unallocatedPizzasPQ by one "+unallocatedPizzasPQ.size());
                        Pizza nextPizza1 = unallocatedPizzasPQ.poll();
                        Pizza nextPizza2 = unallocatedPizzasPQ.poll();
//                        System.out.println("after reducing by one "+unallocatedPizzasPQ.size());
                        Team nextTeamOfTwo = unallocatedTeamsOfTwo.poll();
//                        System.out.println("Allocating nextPizza1 to a team of two "+nextPizza1.toString());
//                        System.out.println("Also allocating nextPizza2 to a team of two "+nextPizza2.toString());
//                        System.out.println(" team of two being "+nextTeamOfTwo.toString());
                        nextTeamOfTwo.allocatePizza(nextPizza1);
                        nextTeamOfTwo.allocatePizza(nextPizza2);
                        nextTeamOfTwo.ingredientsAlreadyServed.addAll(nextPizza1.ingredients);
                        nextTeamOfTwo.ingredientsAlreadyServed.addAll(nextPizza2.ingredients);

                        allocatedTeamsOfTwo.add(nextTeamOfTwo);
                        d.allocatedTeamsOfTwo.add(nextTeamOfTwo);
                        copyDeliveriesToChild(d);
                        d.score += nextTeamOfTwo.ingredientsAlreadyServed.size();
                        neighbours.add(d);
                    }
                }
                else if (remainingPizzasCount == 3){
//                    System.out.println("3 Remaining pizzas");
//                    System.out.println("unallocatedTeamsOfFour.size()"+unallocatedTeamsOfFour.size());
                    if (unallocatedTeamsOfFour.isEmpty()){
//                        System.out.println("unallocatedTeamsOfThree.size()"+unallocatedTeamsOfThree.size());
                        if (!unallocatedTeamsOfThree.isEmpty()){
//                            System.out.println("reducing unallocatedPizzasPQ by one "+unallocatedPizzasPQ.size());
                            Pizza nextPizza1 = unallocatedPizzasPQ.poll();
//                            System.out.println("popped pizza"+nextPizza1.toString());
                            Pizza nextPizza2 = unallocatedPizzasPQ.poll();
//                            System.out.println("popped pizza"+nextPizza2.toString());
                            Pizza nextPizza3 = unallocatedPizzasPQ.poll();
//                            System.out.println("popped pizza"+nextPizza3.toString());
//                            System.out.println("after reducing by one "+unallocatedPizzasPQ.size());
                            Team nextTeamOfThree = unallocatedTeamsOfThree.poll();
//                            System.out.println("Allocating pizza to a team of one");
                            nextTeamOfThree.allocatePizza(nextPizza1);
                            nextTeamOfThree.allocatePizza(nextPizza2);
                            nextTeamOfThree.allocatePizza(nextPizza3);
                            nextTeamOfThree.ingredientsAlreadyServed.addAll(nextPizza1.ingredients);
                            nextTeamOfThree.ingredientsAlreadyServed.addAll(nextPizza2.ingredients);
                            nextTeamOfThree.ingredientsAlreadyServed.addAll(nextPizza3.ingredients);

                            allocatedTeamsOfThree.add(nextTeamOfThree);
                            copyDeliveriesToChild(d);
                            d.score += nextTeamOfThree.ingredientsAlreadyServed.size();
                            neighbours.add(d);
                        }
                    }
                    if (!unallocatedTeamsOfFour.isEmpty()){
                        Pizza nextPizza1 = unallocatedPizzasPQ.poll();
                        Pizza nextPizza2 = unallocatedPizzasPQ.poll();
                        Team nextTeamOfTwo = unallocatedTeamsOfFour.poll();
                        nextTeamOfTwo.allocatePizza(nextPizza1);
                        nextTeamOfTwo.allocatePizza(nextPizza2);
                        nextTeamOfTwo.ingredientsAlreadyServed.addAll(nextPizza1.ingredients);
                        nextTeamOfTwo.ingredientsAlreadyServed.addAll(nextPizza2.ingredients);

                        allocatedTeamsOfThree.add(nextTeamOfTwo);
                        d.allocatedTeamsOfThree.add(nextTeamOfTwo);
                        copyDeliveriesToChild(d);
                        d.score += nextTeamOfTwo.ingredientsAlreadyServed.size();
                        neighbours.add(d);
                    }
                }
                else if (remainingPizzasCount == 2){
//                    System.out.println("2 Remaining pizza");
//                    System.out.println("unallocatedTeamsOfThree.size()"+unallocatedTeamsOfThree.size());
                    if (!unallocatedTeamsOfTwo.isEmpty()){
//                        System.out.println("reducing unallocatedPizzasPQ by one "+unallocatedPizzasPQ.size());
                        Pizza nextPizza1 = unallocatedPizzasPQ.poll();
                        Pizza nextPizza2 = unallocatedPizzasPQ.poll();
//                        System.out.println("after reducing by one "+unallocatedPizzasPQ.size());
                        Team nextTeamOfTwo = unallocatedTeamsOfTwo.poll();
//                        System.out.println("Allocating pizza to a team of one");
                        nextTeamOfTwo.allocatePizza(nextPizza1);
                        nextTeamOfTwo.allocatePizza(nextPizza2);
                        nextTeamOfTwo.ingredientsAlreadyServed.addAll(nextPizza1.ingredients);
                        nextTeamOfTwo.ingredientsAlreadyServed.addAll(nextPizza2.ingredients);

                        allocatedTeamsOfTwo.add(nextTeamOfTwo);
                        d.allocatedTeamsOfTwo.add(nextTeamOfTwo);
                        copyDeliveriesToChild(d);
                        d.score += nextTeamOfTwo.ingredientsAlreadyServed.size();
                        neighbours.add(d);
                    }
                } else return neighbours;
            }
//            System.out.println("ikona neiighbours " + neighbours.size());
            return neighbours;
        }
    }

    public class SortPizzasByDiversity implements Comparator<Pizza> {
        //factor book scores
        public int compare(Pizza a, Pizza b){
            return a.ingredients.size() - b.ingredients.size();
        }
    }

    public class SortTeamsByDiversity implements Comparator<Team> {
        //factor book scores
        public int compare(Team a, Team b){
            return a.ingredientsAlreadyServed.size() - b.ingredientsAlreadyServed.size();
        }
    }

    public void countIngredientIncidences(String filePath) throws FileNotFoundException {
        File file = new File(filePath);
        Scanner input = new Scanner(file);
        int numOfPizzas = input.nextInt();
        int numOfTeamsOfTwo = input.nextInt();
        int numOfTeamsOfThree = input.nextInt();
        int numOfTeamsOfFour = input.nextInt();

        for (int i = 0; i < numOfPizzas; i++){
            Pizza pizza = new Pizza(i);
            int numOfIngredients = input.nextInt();
            for (int j = 0; j < numOfIngredients; j++){
                String ingredient = input.next();
                Integer count = ingredientTypeCount.get(ingredient);
                if (count == null) count = 0;
                ingredientTypeCount.put(ingredient, count+1);
            }
        }

        for (Integer value : ingredientTypeCount.values()){
            this.ingredientAppearances += value;
        }

        //Reset input to pizzas
        input = new Scanner(file);
        input.nextLine();

        for (int i = 0; i < numOfPizzas; i++){
            Pizza pizza = new Pizza(i);
            int numOfIngredients = input.nextInt();
            for (int j = 0; j < numOfIngredients; j++){
                String ingredient = input.next();
                pizza.addIngredient(ingredient);
            }
            pizza.calculateRarity();
            seedDelivery.unallocatedPizzasPQ.add(pizza);
            seedDelivery.score += pizza.ingredients.size();
        }

        for (int i = 0; i < numOfTeamsOfTwo; i++){
            Team t = new Team(2);
            seedDelivery.unallocatedTeamsOfTwo.add(t);
        }

        for (int i = 0; i < numOfTeamsOfThree; i++){
            Team t = new Team(3);
            seedDelivery.unallocatedTeamsOfThree.add(t);
        }

        for (int i = 0; i < numOfTeamsOfFour; i++){
            Team t = new Team(4);
//            System.out.println("unallocated teams of four inaallocatiwa");
            seedDelivery.unallocatedTeamsOfFour.add(t);
        }

    }

    public static void main(String[] args) throws FileNotFoundException {
        String filePath = "/home/lobi/IdeaProjects/I/src/a_example";
        HashCode2021Test hashCode2021 = new HashCode2021Test();
        hashCode2021.countIngredientIncidences(filePath);
        hashCode2021.run(filePath);


        //Remember to test removePizza and add deepcopy if needed
    }


    public void run(String filePath) throws FileNotFoundException {

//        System.out.println("number of neighbours");
        //???? dafuq????
//        sampleDelivery.neighbors();
//        System.out.println(sampleDelivery.neighbors().size());
        PriorityQueue<Delivery> bestSolution = new PriorityQueue<>();
        int addition = 0;
        addition = addition+1;
        bestSolution.add(seedDelivery);
        Delivery nextBestDelivery = bestSolution.peek();
//        System.out.println("SEED DELIVERY");
//        printDelivery(nextBestDelivery);
//        System.out.println(addition+" addition");
        while (!bestSolution.isEmpty()){
//            System.out.println("imeingia");
            nextBestDelivery = bestSolution.poll();
            PriorityQueue nextBestDeliveryNeighbours = nextBestDelivery.neighbors();
//            System.out.println("imecalculate neighbours ya "+(addition));
//            System.out.println("nextBestDeliveryNeighbours.size()"+nextBestDeliveryNeighbours.size());
            String toString = nextBestDelivery.toString();
//            System.out.println("Delivery "+toString.substring(17, toString.length()));
//            printDelivery(nextBestDelivery);
            Iterator<Delivery> it = nextBestDeliveryNeighbours.iterator();
            while (it.hasNext()){

//                System.out.println("Children belonging to "+toString.substring(17, toString.length()));
                addition = addition+1;
                Delivery nextDelivery = it.next();
                toString = nextDelivery.toString();
                String toString2 = nextBestDelivery.toString();
//                System.out.println("Delivery "+toString.substring(17, toString.length()) + " with parent"+toString2.substring(17, toString2.length()));
//                printDelivery(nextDelivery);
//                System.out.println("... yet ");
//                System.out.println("nextDelivery.allocatedTeamsOfTwo.size()"+nextDelivery.allocatedTeamsOfTwo.size());
//                System.out.println("nextDelivery.allocatedTeamsOfFour.size()"+nextDelivery.allocatedTeamsOfFour.size());
//                System.out.println("nextDelivery.allocatedTeamsOfThree.size()"+nextDelivery.allocatedTeamsOfThree.size());
//                System.out.println("nextBestDelivery.unallocatedPizzasPQ.size()"+nextBestDelivery.unallocatedPizzasPQ.size());
//                System.out.println(addition+" addition");
                PriorityQueue<Delivery> neighbours = nextDelivery.neighbors();
                Iterator<Delivery> ite = neighbours.iterator();
                while (ite.hasNext()){
                    Delivery nd = ite.next();
                    System.out.println("Delivery "+nd.toString().substring(17, 34) + " with parent"+nextDelivery.toString().substring(17, 34));
//                    printDelivery(nd);
                    bestSolution.add(nd);
                }

            }
        }
        int sum = 0;
        Iterator<Team> itD;
        itD = nextBestDelivery.allocatedTeamsOfTwo.iterator();
        while (itD.hasNext()){
            sum = itD.next().ingredientsAlreadyServed.size();
        }
        itD = nextBestDelivery.allocatedTeamsOfThree.iterator();
        while (itD.hasNext()){
            sum = itD.next().ingredientsAlreadyServed.size();
        }
        itD = nextBestDelivery.allocatedTeamsOfFour.iterator();
        while (itD.hasNext()){
            sum = itD.next().ingredientsAlreadyServed.size();
        }
        System.out.println("Score: "+(sum*sum));
        System.out.println("---END---");
        System.out.println(nextBestDelivery.allocatedTeamsOfThree.size()+
                nextBestDelivery.allocatedTeamsOfTwo.size() +
                nextBestDelivery.allocatedTeamsOfFour.size()
        );
        if (nextBestDelivery.allocatedTeamsOfTwo.size() > 0){

            Iterator<Team> it = nextBestDelivery.allocatedTeamsOfTwo.iterator();
            while(it.hasNext()){
                Team t = it.next();
                System.out.print("2 ");
                Iterator<Pizza> pizzaIterator = t.allocatedPizzas.iterator();
                while(pizzaIterator.hasNext()) {
                    Pizza p = pizzaIterator.next();
                    System.out.print(p.index + " ");
                }
                System.out.println();
            }
        }
        if (nextBestDelivery.allocatedTeamsOfThree.size() > 0){

            Iterator<Team> it = nextBestDelivery.allocatedTeamsOfThree.iterator();
            while(it.hasNext()){
                Team t = it.next();
                System.out.print("3 ");
                Iterator<Pizza> pizzaIterator = t.allocatedPizzas.iterator();
                while(pizzaIterator.hasNext()) {
                    Pizza p = pizzaIterator.next();
                    System.out.print(p.index + " ");
                }
                System.out.println();
            }
        }
        if (nextBestDelivery.allocatedTeamsOfFour.size() > 0){

            Iterator<Team> it = nextBestDelivery.allocatedTeamsOfFour.iterator();
            while(it.hasNext()){
                Team t = it.next();
                System.out.print("4 ");
                Iterator<Pizza> pizzaIterator = t.allocatedPizzas.iterator();
                while(pizzaIterator.hasNext()) {
                    Pizza p = pizzaIterator.next();
                    System.out.print(p.index + " ");
                }
                System.out.println();
            }
        }
//        try (PrintWriter output = new PrintWriter("output" + ".out", "UTF-8")) {
//            output.println(out.size());
//            System.out.println(out.size() + " - " +total);
//            for (String outputLine : out) {
//                output.println(outputLine );
//                System.out.println(outputLine );
//            }
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }

        System.out.println("");
//        File file = new File("input.txt");
//        Scanner input = new Scanner(file);
//        numberOfPizzas = input.nextInt();
//        int teamsOfOneCount = input.nextInt();
//        int teamsOfTwoCount = input.nextInt();
//        int teamsOfThreeCount = input.nextInt();
//        for (int i = 0; i < teamsOfOneCount; i++){
//            unallocatedTeamsOfThree.add(new Team(1));
//        }
//        for (int i = 0; i < teamsOfTwoCount; i++){
//            unallocatedTeamsOfFour.add(new Team(2));
//        }
//        for (int i = 0; i < teamsOfThreeCount; i++){
//            unallocatedTeamsOfFour.add(new Team(3));
//        }
//
//        for (int i = 0; i < numberOfPizzas; i++){
//            Pizza p = new Pizza();
//            int numberOfIngredients = input.nextInt();
//            for (int j = 0; j < numberOfIngredients; j++){
//                p.addIngredient(input.next());
//            }
//            unallocatedPizzasPQ.add(p);
//        }
//
//        Delivery d = new Delivery();
//        Team nextTeamOfOne = unallocatedTeamsOfThree.poll();
//        nextTeamOfOne.allocatePizza(unallocatedPizzasPQ.poll());
//        allocatedTeamsOfTwo.add(nextTeamOfOne);
//        Iterator allocatedTeamsOfTwoIterator = allocatedTeamsOfTwo.iterator();
//        while (allocatedTeamsOfTwoIterator.hasNext()){
//            Team nextTeam = (Team) allocatedTeamsOfTwoIterator.next();
//            d.teamsOfOne.add(nextTeam);
//        }
//
//        deliveriesPQ.add(d);
//
//        while (deliveriesPQ.size() > 0){
//
//            while (unallocatedPizzasPQ.size() > 0){
//
//            }
//        }
    }
    public void printDelivery(Delivery d){
        System.out.println("-----------------------------------------------");
        System.out.println("------------------All Pizzas-------------------");
        Iterator<Pizza> itP = d.unallocatedPizzasPQ.iterator();
        String toString;
        while (itP.hasNext()){
            Pizza p = itP.next();
            toString = p.toString();
            System.out.print(toString.substring(17, toString.length())+", ");
        }
        System.out.println();
        System.out.println("   TEAMS OF 2       ||      PIZZAS             ");
        Iterator<Team> itT = d.allocatedTeamsOfTwo.iterator();
        while (itT.hasNext()){
            Team t = itT.next();
            toString = t.toString();
            System.out.print(toString.substring(17, toString.length())+"  ::  ");
            itP = t.allocatedPizzas.iterator();
            while (itP.hasNext()){
                toString = itP.next().toString();
                System.out.print(toString.substring(17, toString.length())+", ");
            }
        }
        System.out.println("");
        System.out.println("   TEAMS OF 3       ||      PIZZAS             ");
        itT = d.allocatedTeamsOfThree.iterator();
        while (itT.hasNext()){
            Team t = itT.next();
            toString = t.toString();
            System.out.print(toString.substring(17, toString.length())+"  ::  ");
            itP = t.allocatedPizzas.iterator();
            while (itP.hasNext()){
                toString = itP.next().toString();
                System.out.print(toString.substring(17, toString.length())+", ");
            }
        }
        System.out.println("");
        System.out.println("   TEAMS OF 4       ||      PIZZAS             ");
        itT = d.allocatedTeamsOfFour.iterator();
        while (itT.hasNext()){
            Team t = itT.next();
            toString = t.toString();
            System.out.print(toString.substring(17, toString.length())+"  ::  ");
            itP = t.allocatedPizzas.iterator();
            while (itP.hasNext()){
                toString = itP.next().toString();
                System.out.print(toString.substring(17, toString.length())+", ");
            }
        }
        System.out.println("");
    }
}